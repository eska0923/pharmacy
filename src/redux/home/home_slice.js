import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// import ApiService from "../../http";

export const homeSlice = createSlice({
  name: "home",
  initialState: {
    data: [],
    selectedItem: null,
    selectedItems: [],
    loading: true,
    sumPrice: 0,
  },
  reducers: {
    selectItem: (state, action) => {
      const activeItem = state.selectedItems.find(
        (e) => e.barcode === action.payload
      );
      if (activeItem == null) {
        state.selectedItem = state.data.find(
          (e) => e.barcode === action.payload
        );
        state.selectedItem.count = 1;
        state.selectedItem.unitPrice = getUnitPrice(
          state.selectedItem.price,
          state.selectedItem.count
        );
        state.selectedItems.push(state.selectedItem);
      } else {
        activeItem.count = activeItem.count + 1;
        activeItem.unitPrice = getUnitPrice(activeItem.price, activeItem.count);
      }
    },
    removeItem: (state, action) => {
      const newData = state.selectedItems.filter(
        (item) => item.barcode !== action.payload
      );
      if (state.selectedItem.barcode === action.payload) {
        state.selectedItem = null;
      }
      state.selectedItems = newData;
    },
    changeCount: (state, action) => {
      console.log("action", action);
      const newData = [...state.selectedItems];
      const index = newData.findIndex(
        (item) => item.barcode === action.payload.barcode
      );
      state.selectedItems[index].count = parseInt(action.payload.count);
      state.selectedItems[index].unitPrice = getUnitPrice(
        state.selectedItems[index].price,
        state.selectedItems[index].count
      );
    },
    calcPrice: (state) => {
      const sum = state.selectedItems
        .map((e) => e.unitPrice)
        .reduce((partialSum, a) => partialSum + a, 0);

      state.sumPrice = sum;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getDrugs.pending, (state) => {
        state.loading = true;
        state.data = [];
      })
      .addCase(getDrugs.fulfilled, (state) => {
        state.loading = false;
        state.data = [
          {
            barcode: "820017710435",
            name: "Эксфорж",
            sku: "820017710435",
            brand: "NOVARTIS",
            description:
              "Даралт бууруулах эм: Нэгэн төрлийн эмээр даралт буухгүй байгаа хүмүүст тохиромжтой. 2 эмийн хавсарсан найрлагатай",
            price: 22400,
          },
          {
            barcode: "5997001333013",
            sku: "5997001333013",
            name: "Бидоп 10мг №28 Гедеон",
            brand: "GEDEONRIHTER",
            description:
              "Зүрх судасны эм: Зүрхний ишемийн эмгэг: тогтвортой хэлбэрийн зүрхний бахын хөдлөлтөөс сэргийлэх зорилгоор хэрэглэнэ. Зүрхний архаг дутагдал. Цусны даралт ихсэх эмгэг ба зүрхний ишемийн үед.Зүрхний архаг дутагдал үед хэрэглэнэ",
            price: 30500,
          },
          {
            barcode: "5997001359181",
            name: "Микосист 100 мг №28 Гедеон",
            sku: "5997001359181",
            brand: "GEDEONRIHTER",
            description:
              "Мөөгөнцрийн эсрэг өргөн хүрээний үйлдэлтэй.Бэлдмэлийг ууж хэрэглэхэд хоол боловсруулах замаар сайн шимэгдэх бөгөөд биохүртэхүй нь судсанд тарьж хэрэглэсэн тунгийн 90%-аас дээш тунтай тэнцэнэ.Микосистийг ууж хэрэглэснээс 1-2 цагийн дараа цусны сийвэн дэхь концентраци нь дээд хэмжээндээ хүрэх бөгөөд хагас ялгарлын үе нь ойролцоогоор 30 цаг байна.Хамт хэрэглэж буй хоол хүнсний зүйл бэлдмэлийн шимэгдэлтэнд нөлөөлөхгүй",
            price: 105000,
          },
          {
            barcode: "3838989566067",
            name: "Макропен суспенз 175мг/5мл №1",
            sku: "3838989566067",
            brand: "KRKA",
            description:
              "Макропен – нянгийн эсийн уургийн нийлэгжилтийг дарангуйлдаг макролидын бүлгийн антибиотик.",
            price: 30500,
          },
        ];
      })
      .addCase(getDrugs.rejected, (state) => {
        state.loading = true;
        state.data = [];
      });
  },
});

const getUnitPrice = (price, count) => {
  return count * price;
};
export const getDrugs = createAsyncThunk("getDrugs", async () => {
  // const data = await ApiService("get", `courses`);
  // console.log("response data", data);
  return [
    {
      barcode: "820017710435",
      name: "Эксфорж",
      sku: "820017710435",
      brand: "NOVARTIS",
      description:
        "Даралт бууруулах эм: Нэгэн төрлийн эмээр даралт буухгүй байгаа хүмүүст тохиромжтой. 2 эмийн хавсарсан найрлагатай",
      price: 22400,
    },
    {
      barcode: "5997001333013",
      sku: "5997001333013",
      name: "Бидоп 10мг №28 Гедеон",
      brand: "GEDEONRIHTER",
      description:
        "Зүрх судасны эм: Зүрхний ишемийн эмгэг: тогтвортой хэлбэрийн зүрхний бахын хөдлөлтөөс сэргийлэх зорилгоор хэрэглэнэ. Зүрхний архаг дутагдал. Цусны даралт ихсэх эмгэг ба зүрхний ишемийн үед.Зүрхний архаг дутагдал үед хэрэглэнэ",
      price: 30500,
    },
    {
      barcode: "5997001359181",
      name: "Микосист 100 мг №28 Гедеон",
      sku: "5997001359181",
      brand: "GEDEONRIHTER",
      description:
        "Мөөгөнцрийн эсрэг өргөн хүрээний үйлдэлтэй.Бэлдмэлийг ууж хэрэглэхэд хоол боловсруулах замаар сайн шимэгдэх бөгөөд биохүртэхүй нь судсанд тарьж хэрэглэсэн тунгийн 90%-аас дээш тунтай тэнцэнэ.Микосистийг ууж хэрэглэснээс 1-2 цагийн дараа цусны сийвэн дэхь концентраци нь дээд хэмжээндээ хүрэх бөгөөд хагас ялгарлын үе нь ойролцоогоор 30 цаг байна.Хамт хэрэглэж буй хоол хүнсний зүйл бэлдмэлийн шимэгдэлтэнд нөлөөлөхгүй",
      price: 105000,
    },
    {
      barcode: "3838989566067",
      name: "Макропен суспенз 175мг/5мл №1",
      sku: "3838989566067",
      brand: "KRKA",
      description:
        "Макропен – нянгийн эсийн уургийн нийлэгжилтийг дарангуйлдаг макролидын бүлгийн антибиотик.",
      price: 30500,
    },
  ];
});

// Action creators are generated for each case reducer function
export const { selectItem, removeItem, changeCount, calcPrice } =
  homeSlice.actions;

export default homeSlice.reducer;
