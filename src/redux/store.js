import { configureStore } from "@reduxjs/toolkit";
import homeReducer from "./home/home_slice";
export default configureStore({
  reducer: {
    home: homeReducer,
  },
});
