import Home from "./pages/home";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import Report from "./pages/report";

const { Header, Footer, Content } = Layout;

function App() {
  return (
    <BrowserRouter>
      <Layout className="layout">
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="1">
              <Link to={"/"}>Нүүр</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to={"/report"}>Тайлан</Link>
            </Menu.Item>
            <Menu.Item key="3">Гарах</Menu.Item>
          </Menu>
        </Header>

        <Content style={{ padding: "0 50px" }}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/report" element={<Report />} />
          </Routes>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Pharma ©2022 Created by Erka
        </Footer>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
