import axios from "axios";
//api huselt gargaj baiga neg l gartstai
import { NotificationManager } from "react-notifications";
const baseURL = "http://localhost:8000/api/v1";
const alertTimeOut = 2 * 1000; //alertiin haragdah hugatsaa
const instance = axios.create({
  baseURL,
  withCredentials: true,
});
instance.defaults.withCredentials = true;
const ApiService = (method, url, data, headers = {}) => {
  const options = {
    method: method,
    url: url,
    ...(data?._download && { responseType: "blob" }),

    headers: {
      "Content-Type": "application/json", //'application/octet-stream'
      // Authorization: "Bearer " + localStorage?.getItem("token"),
      ...headers,
    },
  };
  if (typeof window !== "undefined") {
    // your code
    // console.log("get token", getCookie("ez4learn-token"));
    // options.headers = {
    //   ...options.headers,
    //   Authorization: "Bearer " + getCookie("ez4learn-token"),
    // };
    console.log("get token", localStorage?.getItem("token"));
    options.headers = {
      ...options.headers,
      Authorization: "Bearer " + localStorage?.getItem("token"),
    };

    console.log("storage", options);
  }

  // function getCookie(name) {
  //   const value = `; ${document.cookie}`;
  //   const parts = value.split(`; ${name}=`);
  //   if (parts.length === 2) return parts.pop().split(";").shift();
  // }
  if (method.toLowerCase() === "get") options.params = data;
  else options.data = data;

  const onSuccess = function (response) {
    if (data?._download) {
      //file tatah ued ashiglana duriin file tatdag bolgoh
      // const FileDownload = require("js-file-download");
      // let filename = `report${Math.floor(Math.random() * 100000)}.xlsx`;
      // FileDownload(response.data, filename);
      // return false;
    }

    if (data?.success === false) {
      try {
        NotificationManager.error(
          data?.message || "Сэрвэртэй холбогдоход алдаа гарлаа",
          "",
          alertTimeOut,
          () => {}
        );
      } catch (ex) {
        NotificationManager.error(
          "Сэрвэртэй холбогдоход алдаа гарлаа",
          "",
          alertTimeOut,
          () => {}
        );
      }

      Promise.reject(303 || data?.message);
    }
    console.log("data", response);
    return response.data;
  };

  const onError = function (error) {
    //axiosoosoo garsan aldaa

    console.log("error", error.response.status);
    console.log("error response", error.message);
    try {
      NotificationManager.error(
        error.response.data.error.message ||
          "Сэрвэртэй холбогдоход алдаа гарлаа",
        "",
        alertTimeOut,
        () => {}
      );
    } catch (ex) {
      NotificationManager.error(
        "Сэрвэртэй холбогдоход алдаа гарлаа",
        "",
        alertTimeOut,
        () => {}
      );
    }
    if (error && error.response && error.response.status === 401) {
      //useriin medeelel tseverleh
      // window.location.href = "/";
    }
    return Promise.reject(error.response || error.message || "Алдаа гарлаа");
  };
  // instance.defaults.withCredentials = true;

  return instance(options).then(onSuccess).catch(onError);
};

export default ApiService;
