const formatter = (value) => {
  return Intl.NumberFormat("mn-MN").format(value) + " ₮";
};
export default formatter;
