import React, { useEffect, useRef } from "react";
import { Form, Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { selectItem } from "../../../redux/home/home_slice";
const AddForm = () => {
  const { data, selectedItem } = useSelector((state) => state.home);
  const dispatch = useDispatch();
  const formRef = useRef(null);
  const { TextArea } = Input;
  const onFinish = (values) => {
    console.log("Success:", values);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onChange = (value) => {
    console.log(`selected ${value}`);
    dispatch(selectItem(value));
  };

  const onSearch = (value) => {
    console.log("search:", value);
  };
  useEffect(() => {
    console.log("selectedItem", selectedItem);
    if (selectedItem != null)
      formRef.current?.setFieldsValue({
        ...selectedItem,
      });
    else {
      formRef.current?.setFieldsValue({
        barcode: "",
        name: "",
        sku: "",
        price: "",
        description: "",
      });
    }
  }, [selectedItem]);
  return (
    <Form
      ref={formRef}
      name="add form"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item label="Эмин баркод" name="barcode">
        <Select
          showSearch
          placeholder=""
          allowClear
          optionFilterProp="children"
          onChange={onChange}
          onSearch={onSearch}
          filterOption={(input, option) =>
            (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
          }
          options={data.map((e) => {
            return {
              value: e.barcode,
              label: e.barcode,
            };
          })}
        />
      </Form.Item>
      <Form.Item label="Sku code" name="sku">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Эмийн нэр" name="name">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Эмийн үнэ" name="price">
        <Input disabled />
      </Form.Item>
      {/* <Form.Item label="Эмийн төрөл" name="type">
        <Input disabled />
      </Form.Item> */}
      <Form.Item label="Эмийн тайлбар" name="description">
        <TextArea disabled rows={6} />
      </Form.Item>
    </Form>
  );
};
export default AddForm;
