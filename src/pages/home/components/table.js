import React, { useContext, useEffect, useRef, useState } from "react";
import { Button, Form, Input, Table } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { changeCount, removeItem } from "../../../redux/home/home_slice";
import formatter from "../../../util/price_formatter";

const EditableContext = React.createContext(null);
const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};
const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);
  const toggleEdit = (barcode, count) => {
    setEditing(!editing);
    form.setFieldsValue({
      [barcode]: count,
    });
  };
  const save = async () => {
    try {
      const values = await form.validateFields();
      console.log("values", values);
      const data = Object.entries(values)[0];
      const barcode = data[0];
      const count = data[1];
      toggleEdit(barcode, count);
      handleSave({
        barcode,
        count,
      });
    } catch (errInfo) {
      console.log("Save failed:", errInfo);
    }
  };
  let childNode = children;
  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={record.barcode}
        rules={[
          {
            message: "0-с их тоо оруулна уу",
            validator: (_, value) => {
              console.log("value", value);
              if (value > 0) {
                return Promise.resolve();
              } else {
                return Promise.reject("0-с их тоо оруулна уу");
              }
            },
          },
        ]}
      >
        <Input
          ref={inputRef}
          type="number"
          onPressEnter={save}
          onBlur={save}
          min={0.1}
        />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }
  return <td {...restProps}>{childNode}</td>;
};
const CardList = () => {
  const dataSource = useSelector((state) => state.home.selectedItems);
  const dispatch = useDispatch();
  const handleDelete = (record) => {
    dispatch(removeItem(record.barcode));
  };
  const defaultColumns = [
    {
      title: "Эмийн баркод",
      dataIndex: "barcode",
      width: "10%",
    },
    {
      title: "Эмийн нэр",
      dataIndex: "name",
      width: "20%",
    },
    {
      title: "Эмийн тоо",
      dataIndex: "count",
      editable: true,
    },
    {
      title: "Эмийн үнэ",
      dataIndex: "price",
    },
    {
      title: "Нийт үнэ",
      dataIndex: "unitPrice",
      render: (_, record) => formatter(record.unitPrice),
    },
    {
      title: "",
      dataIndex: "operation",
      render: (_, record) =>
        dataSource.length >= 1 ? (
          <Button
            type="primary"
            onClick={() => {
              handleDelete(record);
            }}
            danger
            icon={<DeleteOutlined />}
          >
            Хасах
          </Button>
        ) : null,
    },
  ];
  const handleSave = ({ barcode, count }) => {
    dispatch(
      changeCount({
        barcode,
        count,
      })
    );
  };
  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };
  const columns = defaultColumns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });
  return (
    <div>
      <Table
        components={components}
        rowClassName={() => "editable-row"}
        bordered
        dataSource={dataSource}
        columns={columns}
      />
    </div>
  );
};
export default CardList;
