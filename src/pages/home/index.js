import { Row, Col, Typography } from "antd";
import { useEffect } from "react";
import CustomCard from "../../components/card";
import AddForm from "./components/form";
import { useDispatch, useSelector } from "react-redux";
import { getDrugs, calcPrice } from "../../redux/home/home_slice";
import CardList from "./components/table";
import formatter from "../../util/price_formatter";
const Home = () => {
  const dispatch = useDispatch();
  const { loading, selectedItems, sumPrice } = useSelector(
    (state) => state.home
  );
  const { Text } = Typography;
  useEffect(() => {
    dispatch(getDrugs());
  }, [dispatch]);
  useEffect(() => {
    dispatch(calcPrice());
  }, [selectedItems, dispatch]);
  return (
    <>
      {loading && "Unshij baina"}
      <Row style={{ minHeight: 280, display: "flex", gap: 20, marginTop: 10 }}>
        <Col sm={7}>
          <CustomCard title={"Бараа"} style={{ width: "100%" }}>
            <AddForm></AddForm>
          </CustomCard>
        </Col>
        <Col sm={16}>
          <CustomCard title={"Авсан бараа"} style={{ width: "100%" }}>
            <CardList></CardList>
            <div
              style={{
                gap: 10,
                display: "flex",
                justifyContent: "end",
              }}
            >
              <Text style={{ fontSize: 20 }} underline>
                Нийт дүн:
              </Text>
              <Text style={{ fontSize: 20, fontWeight: "bold" }} type="success">
                {formatter(sumPrice)}
              </Text>
            </div>
          </CustomCard>
        </Col>
      </Row>
    </>
  );
};

export default Home;
