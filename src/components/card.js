import React from "react";
import { Card } from "antd";
const CustomCard = ({ children, title, style }) => (
  <div className="site-card-border-less-wrapper">
    <Card title={title} bordered={false} style={style}>
      {children}
      {/* <p>Card content</p>
      <p>Card content</p>
      <p>Card content</p> */}
    </Card>
  </div>
);
export default CustomCard;
